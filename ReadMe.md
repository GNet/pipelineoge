|-----------------------------------------| <br>
| PIPELINE Organellar Gene Expression |<br>
|-----------------------------------------| <br>

This pipeline is designed to easily perform the bioinformatic and differential analysis steps to analyze the nuclear gene expression but also the organellar gene expression including its specificities such as C-to-U RNA editing, splicing and "processing", from RNAseq data. <br>
This pipeline performs read mapping and generates count tables of gene expression, splicing (Pt/Mt only), editing (Pt/Mt only) and coverage (Pt/Mt only) for every samples. It can also conduct differential analysis on nuclear and organellar gene expression, organellar C-to-U editing rate and organellar splicing rate. <br>
<br>
The pipeline provides BAM files, raw count tables, merged raw count tables, plots and result tables of differential analysis. A log file is also generated. <br>
It can analyze 1 to n samples (fastq.gz files) sequenced in paired-end or in single-end. <br>
However, the differential analysis can only be conducted for one biological factor with two modalities (for example, wild-type vs mutant or control vs treatment), and one technical factor (i.e. Replicate). <br>
<br>
This pipeline is an automatized version, generalized to mitochondrion, of the command lines provided in Malbert _et al._, 2018. Differential analysis of gene expression is conducted as in DiCoExpress (Lambert _et al._, 2020). <br>

# INSTALLATION INSTRUCTIONS
## Requirements
- Some softwares are required by the pipeline and must be installed before running an analysis :

	* STAR
	* samtools
	* bedtools
	* subread (featureCounts)
	* python3
	* R and packages: edgeR, FactoMineR, gplots, ggpubr

- **Fasta and GTF** files must contain sequences and at least the exon annotations for plastidial and mitochondrial chromosomes. 
- Plastidial and mitochondrial chromosome's names must be known. To know chromosome names, the following commands can be used :
	* Names in the Fasta file : `grep ">" path/to/Fasta`
	* Names in the GTF file : `cut -f1 path/to/GTF | sort | uniq`

## Installation
- Download the pipelineOGE/ folder on your server.
- Be sure that all required softwares and packages are installed.
- Create the python virtual environment in the pipelineOGE folder and install pysamstats. <br>
**/!\\** Be careful: this virtual environment must be named "MyPython" and be created in the pipelineoge/pipelineOGE/ folder.
	* Set the pipelineoge/pipelineOGE/ folder as your working directory.
	* Create the virtual environment: `virtualenv MyPython`. It has to be named "MyPython".
	* Activate it: `source MyPython/bin/activate`. You can check the activation with `which python`, the path should show the path to the MyPython folder.
	* Install pysam: `pip install pysam==0.15.4`
	* Install pysamstats: `pip install pysamstats`. You can check the installation with `pysamstats --help`.
	* Deactivate the environment: `deactivate`
- Follow the guidelines in the "REFERENCE SET-UP" part for each species you want to analyze.

# REFERENCE SET-UP
A reference set is a set of files required by the pipeline and is specific to each species to be analyzed. <br>
It is composed of one STAR index, one GTF, one Fasta and one set of splicing files. <br>
The guidelines below have to be followed once for each species you want to analyze with this pipeline.

## step 1 : General set-up
- Get the fasta and annotation files. <https://plants.ensembl.org> is a reference repository where you can find the fasta and annotation files for numerous plant species.
- The annotation file must be in the **GTF** format.
- All chromosomes and contigs fasta sequences must be concatenated in a single multi-fasta file.

## step 2 : Generate the index for STAR
- Create a folder that will contain the index
- Generate the index for STAR using the command: `STAR --runMode genomeGenerate --runThreadN 16 --sjdbOverhang 74 --genomeDir /path/to/indexFolder --genomeFastaFiles /path/to/fasta --sjdbGTFfile /path/to/GTF` 
You have to adapt this command line according to your conditions: --runThreadN is the number of threads to use; --sjdbOverhang should be set to read length -1. For more information see the STAR manual.

## step 3 : Create gff3 files for splicing analysis
The splicing analysis needs 3 gff3 files for each organellar chromosome. These files contain the coordinates of exons, introns and splice sites (i.e. a two bases window around each exon-intron junction). <br>

- The suffix name of these files must be : \_exon.gff3, \_intron.gff3, \_splice_sites.gff3. 
- Files must be in directories with the exact same name as organellar chromosomes, themselves being in the same parent directory.
- These **files can be automatically generated** from the GTF file with the script `/path/to/pipelineOGE/GTFtoSplicingFiles.sh`. Strands "+" and "-" are inverted in the generated files to be used with the sequencing protocol detailed in Baudry _et al._, 2020.
- For help and usage of this script use the command: `/path/to/pipelineOGE/GTFtoSplicingFiles.sh -h`.

**/!\\** Be carefull: `GTFtoSplicingFiles.sh`, assumes that the two first elements of the GTF's 9th field of exons are `gene_id "XXX"; transcript_id "XXX"` in this order and it also must contain the `exon_number "n"` element. You have to check these points before using it but that's the case for the GTF files provided by <https://plants.ensembl.org>. **Note:** outputs of `GTFtoSplicingFiles.sh` are directly written in the working directory.

## step 4 : Register informations in the indexList.txt
- `indexList.txt` is a file listing all your available reference sets (one reference set by row). This file must be at the root of the pipelineOGE/ folder.<br>

This file contains nine tabulated columns : <br>
**/!\\** Be careful: if there is any special characters in path DO NOT escape them. <br>
For example, write `/path/with special characters (1)/` instead of `/path/with\ special\ characters \(1\)/`. <br>

1. ID : unique identifier of the reference set. (should be an integer) 
2. Name : reference set's name. (for human reading only, \<spaces\> are tolerated)
3. details : informations about the reference set. (for human reading only, \<spaces\> are tolerated)
4. path-to-STAR_index : absolute path to the STAR index folder.
5. path-to-GTF_file : absolute path to the GTF file.
6. path-to-FASTA_file : absolute path to the multiFasta file.
7. path-to-Splicing_files : absolute path to the directory containing the gff3 files . This directory must contain sub-directories with the exact same name as organellar chromosomes and containing the 3 gff3 files. 
8. Pt/Mt-name : name of organellar chromosomes. Names must be separated by "/" without any space. Plastidial chromosome name must be in the first place. For example, `Pt/Mt` or `ChrC/ChrM`.
9. STARversion : version of STAR used to create genome index. (for human reading only)

- When running, the pipeline displays the **first three columns** of this file to let the user choose his/her reference set.

# RUN AN ANALYSIS
## step 1 : Create the compressed sample list file (i.e. sampleFile)
- To use the pipeline, samples must be compressed (sample_name.**fastq.gz**).
- To run, the pipeline needs a list of all samples to analyse, named here after the **sampleFile**. <br>
This list is a 4 (or 5 for paired-end) tabulated columns file containing for each sample (one by row), name, absolute path to fastq.gz, modalities of the biological factor and modalities of the technical factor.
	* single-end format : sample name "\t" path-to-fastq.gz "\t" modality of the biological factor "\t" modality of the technical factor
	* paired-end format : sample name "\t" path-to-fastq_R1.gz "\t" path-to-fastq_R2.gz "\t" modality of the biological factor "\t" modality of the technical factor

This file is used both for the bioinformatic step (mapping-counting) and the biostatistical step (differential analysis) of this pipeline. <br>

## step 2 : Run the analysis
- Create a directory in which the analysis will be conducted and set it as your working directory (all output files are stored in the working directory). The sampleFile should be in this working directory.
- Call the pipeline main program with the command: `/path/to/pipelineOGE/pipelineMC_OGE.sh`. Because mapping and counting can take a long time, we recommend to call the pipeline in a `screen`.
- Then provide the informations required for the pipeline:
	* Type the sampleFile's name, then check the sample list and the sample number.
	* Choose if you want the pipeline to perform mapping-counting.
		* Type the kind of sequencing (single-end or paired-end).
		* Choose if you want the pipeline to keep BAM files.
		* Choose the number of threads STAR will use to map.
		* Choose the correct reference set by typing its ID.
	* Choose if you want the pipeline to perform differential analysis.
		* Choose if you want to use the differential analysis default behavior.
		* If not, type your parameter values.
- Each step of the analysis (mapping, counting and differential analysis) is then performed for each sample one by one.

## step 3 : Outputs
The pipeline provides alignment files (.bam, if kept in the options), RAW count tables, results of all differential analyses conducted and two log files (a global log file: pipelineMC_OGE.log and a differential analysis log file: DiffAnalysis.log). <br>
Log files are directly written in the working directory. <br>
Other output files are stored in three created directories:

1. ./BAMfiles/ : contains sampleName.bam (alignment file), sampleName.bam.bai (alignment file index) and sampleName.Log.final.out (STAR mapping summary).
2. ./Counts/ : contains all Count tables (featurecounts, splicing, editing, coverage) for each sample and analysis (sampleName_analysis-name.txt) and a merged count table (Merged_analysis-name.txt).
3. ./Analysis/ : contains three directories, one for each differential analysis: DEG (gene expression), editing and splicing.

# RUN ANOTHER DIFFERENTIAL ANALYSIS
By default, the full set of differential analyses is conducted automatically after the mapping-counting steps. However, if required, it can be performed independently of the mapping-counting steps and with different options. <br>
These analysis can be conducted by command line or "manually" in R console or Rstudio.

## Command lines
- Go to the folder in which `pipelineMC_OGE.sh` was previously called.
- **Run all differential analysis at once:** call the `/path/to/pipelineOGE/pipelineMC_OGE.sh` and choose NOT to perform the mapping-counting and choose to perform the differential analysis, with the wanted behavior. **Note:** in this case, the pipelineMC_OGE.log will be overwritten.
- **Run each analysis individually:** It is also possible to call  the different analysis individually and for only one chromosome, in the case of editing or splicing. Call each script with the appropriate arguments, for help and usage for this different scripts use the following command lines: 
	* `Rscript /path/to/pipelineOGE/Toolbox/DiffAnalysis_DEG.R --help`
	* `Rscript /path/to/pipelineOGE/Toolbox/DiffAnalysis_editing.R --help`
	* `Rscript /path/to/pipelineOGE/Toolbox/DiffAnalysis_splicing.R --help`

## Rstudio or R console
- Open the adapted script from the Toolbox/ folder.
- Set the folder in which `pipelineMC_OGE.sh` was previously called as your working directory.
- Adapt the values provided in the "Manual argument input and defaults values" section of the script.
- Skip the "Command line arguments parsing" section of the script.
- Run all others sections without any change.

# TEST RUN
With this pipeline, we provide a test dataset to check the pipeline. It contains the complete annotation of _Arabidopsis thaliana_ Col0. We changed the mitochondrial chromosome sequence and annotation by the genuine Col0 genome (NCBI: BK010421). In addition, we corrected the plastidial araport11 annotation for the _Ycf3_ and _tRNAS.1_ genes. <br>
After the installation, go through the following steps to test all pipeline's functionalities.

## step 1 : Generate the index for STAR
- Set the pipelineoge/example/Arabidopsis_thaliana_col0 folder as your working directory, then create the `index` folder.
- Generate the index for STAR using the command: `STAR --runMode genomeGenerate --sjdbOverhang 74 --genomeDir ./index --genomeFastaFiles ./Arabidopsis_thaliana.TAIR10.col0.dna.chromosome.all.fa --sjdbGTFfile ./Arabidopsis_thaliana_col0.gtf` 

## step 2 : Generate the splicing files
- Set the pipelineoge/example/Arabidopsis_thaliana_col0 folder as your working directory.
- For the plastidial splicing sites, use the command: `../../pipelineOGE/GTFtoSplicingFiles.sh -c Pt -g Arabidopsis_thaliana_col0.gtf -o AthCol0`
- For the mitochondrial splicing sites, use the command: `../../pipelineOGE/GTFtoSplicingFiles.sh -c Mt -g Arabidopsis_thaliana_col0.gtf -o AthCol0`

## step 3 : run the mapping test
- Set the example/mapping_test folder as your working directory.
- Adapt the absolute path for each sample's fastq.gz in the `sampleFile.txt` file.
- Run the analysis using the command: `../../pipelineOGE/pipelineMC_OGE.sh`.
- Then provide the informations required for the pipeline:
	* Type the sampleFile's name: `sampleFile.txt`
	* Choose to perform mapping and counting: `y`
	* Type the kind of sequencing, here single-end: `se`
	* Choose NOT to keep BAM files to save disk space: `n`
	* Choose the number of threads that STAR will use to map: `15` (you have to adapt this number according to your hardware)
	* Choose the AthCol0 reference set by typing its ID: `1`
	* Choose NOT to perform differential analysis because this test set doesn't include replicates: `n`
- Compare the resulting files to those in the "Reference_Mapping" folder.

## step 4 : run the differential analysis test 
- Set the example/diffanalysis_test folder as your working directory.
- Run the pipeline using the command: `../../pipelineOGE/pipelineMC_OGE.sh`.
	* Type the sampleFile's name: `sampleFile_dyw2.txt`
	* Choose NOT to perform mapping and counting: `n`
	* Choose to perform differential analysis: `y`
	* Choose to use the differential analysis default behavior: `y`
- Compare the results of the "Analysis" folder created with this analysis to the "Reference_Analysis" folder.

# DETAILS
## Bioinformatic
- Mapping is done with STAR with the default parameters except `--readFilesCommand gunzip -c  --outSAMprimaryFlag AllBestScore --outFilterMultimapScoreRange 0  --outSAMtype BAM SortedByCoordinate  --alignIntronMax 1`.
- Gene Expression Counting is done with featureCounts (subread suite) with the `-M -s 2 -t exon -g gene_id` options.
- Splicing and Coverage are done with bash, samtools, bedtools and R.
- Editing is done with pysamstats (installed in the virtual environment within the pipelineOGE/ folder)
- The coverage tables contain the read START number by chromosomal position on plus and minus strands.

## Differential analysis
- **Gene Expression:** Differentially Expressed Gene (DEG) are identified with the same method as in DiCoExpress (Lambert _et al._, 2020).
- **Editing rate and Splicing rate:** Differentially Edited Sites (DES) and Differentially Spliced Introns (DSI) are identified as described in Malbert _et al._, 2018.

# REFERENCES
* Baudry K., Delannoy E. and Colas-des-Francs-Small C. (2020) Quantification of the mitochondrial transcriptome. **Submitted** <br>
* Lambert I., Paysant-Le Roux C., Colella S. and Martin-Magniette M-L. (2020) DiCoExpress: a tool to process multifactorial RNAseq experiments from quality controls to co-expression analysis through differential analysis based on contrasts inside GLM models. Plant Methods, 16. <br>
* Malbert B., Rigaill G., Brunaud V., Lurin C. and Delannoy E. (2018) Bioinformatic Analysis of Chloroplast Gene Expression and RNA Posttranscriptional Maturations Using RNA Sequencing. **In:** Maréchal E. (eds) Plastids. Methods in Molecular Biology, vol 1829. Humana Press, New York, NY. <br>
