#!/bin/bash
LOG=DiffAnalysis.log #logfile
(
#help/bad usage message
usage() { echo "for help: $0 -h"; echo -e "usage: $0 -S <sampleFile's name> [OPTION]\n" 1>&2; exit 1; }
help() { echo "Help:"; echo "usage: $0 -S <sampleFile's name> [OPTION]"; echo -e "\nPerform the differential analysis of gene expression, editing rate and splicing rate between two conditions.\n"; 
echo -e "\t-S \t sampleFile: sampleFile's name used to run mapping-counting. This option corresponds to the \"--sampleFile=\" argument for all R scripts."; echo -e "\t-C \t CPM cutoff: filtering threshold for DEG analysis. This option corresponds to the \"--CPM=\" argument for DiffAnalysis_DEG.R."; echo -e "\t-R \t replicate effect: if T, a replicate term is added in the GLM for DEG analysis (possible values: T or F)(default: T). This option corresponds to the \"--RepEffect=\" argument for DiffAnalysis_DEG.R."; echo -e "\t-c \t filter on coverage: minimum number of edited reads found across samples to keep the position (default: 6). This option corresponds to the \"--filtr.cov=\" argument for DiffAnalysis_editing.R."; echo -e "\t-r \t filter on editing rate: minimum average editing rate by condition to keep the position (default: 0.005). This option corresponds to the \"--filtr.rate=\" argument for DiffAnalysis_editing.R."; echo -e "\t-v \t filter on coverage: minimum number of spliced reads found across samples to keep the intron (default: 6). This option corresponds to the \"--filtr.cov=\" argument for DiffAnalysis_splicing.R."; echo -e "\t-s \t sample subset: selection of a sample subset to reduce at two conditions (provide selected sample indexes separated by "," (e.g. -s 1,2,4,5), sample indexes correspond to the sampleFile line numbers). This option corresponds to the \"--sub=\" argument for all R scripts."; echo -e "\t-o \t output folder name: folder's name of output data for each analysis. This option corresponds to the \"--outName=\" argument for all R scripts."; echo -e "\t-h \t display this help and exit.\n" 1>&2; exit 1; }

#Get options
while getopts S:C:R:c:r:v:o:s:h option
do
	case "${option}" in
		S) sampleFile=${OPTARG};;
		C) CPM=${OPTARG};;
		R) RepEffect=${OPTARG};;
		c) filtrcoved=${OPTARG};;
		r) filtrrate=${OPTARG};;
		v) filtrcovspl=${OPTARG};;
		o) outName=${OPTARG};;
		s) sub=${OPTARG};;
		h) help;;
		*) echo "Error: Bad usage"; usage;;
	esac
done
if [ -z $sampleFile ]
then
	echo "Error: Missing mandatory argument -S"; usage
fi

#Get path to pipeline's directory
script=`readlink -f $0`
source=`dirname $script`

#Differential analysis
Rscript $source/Toolbox/DiffAnalysis_DEG.R --input=Counts/Merged_featurecounts.txt --sampleFile=$sampleFile --CPM=$CPM --RepEffect=$RepEffect --outName=$outName --sub=$sub
Rscript $source/Toolbox/DiffAnalysis_editing.R --input=Counts/Merged_chloro_editing.txt --sampleFile=$sampleFile --filtr.cov=$filtrcoved --filtr.rate=$filtrrate --outName=$outName --sub=$sub
Rscript $source/Toolbox/DiffAnalysis_editing.R --input=Counts/Merged_mito_editing.txt --sampleFile=$sampleFile --filtr.cov=$filtrcoved --filtr.rate=$filtrrate --outName=$outName --sub=$sub
Rscript $source/Toolbox/DiffAnalysis_splicing.R --input=Counts/Merged_chloro_splicing.txt --sampleFile=$sampleFile --filtr.cov=$filtrcovspl --outName=$outName --sub=$sub
Rscript $source/Toolbox/DiffAnalysis_splicing.R --input=Counts/Merged_mito_splicing.txt --sampleFile=$sampleFile --filtr.cov=$filtrcovspl --outName=$outName --sub=$sub

) 2>&1| tee $LOG #create the log file
