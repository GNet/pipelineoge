#!/bin/bash
#help/bad usage message
usage() { echo "for help: $0 -h"; echo -e "usage: $0 -c <Organellar chromosome's name> -g <GTF's name> -o <Output file name prefix> [OPTION]\n" 1>&2; exit 1; }
help() { echo "Help:"; echo "usage: $0 -c <Organellar chromosome's name> -g <GTF's name> -o <Output file name prefix> [OPTION]"; echo -e "\nSplicing analysis requires 3 file.gff3 of coordinates for each chromosome. This script generates files for one chromosome using the annotation.gtf file and stores them in a folder."; echo -e "\n\t-c \t (mandatory) organellar chromosome's name for which files have to be generated, must be spelled exactly as in the gtf file."; echo -e "\t-g \t (mandatory) absolute path to the annotation.gtf file."; echo -e "\t-o \t (mandatory) name of output folder (place in ./) and prefix used for output gff3 files."; echo -e "\t-k \t (optional)  to keep temporary files in ./Splicing_TMP/"; echo -e "\t-h \t display this help and exit.\n" 1>&2; exit 1; }
#Get options
keep="N" #if ==N tmp files will be removed ; ==Y tmp files are kept
while getopts c:g:o:hk option
do
	case "${option}" in
		c) Chr=${OPTARG};;
		g) GTF=${OPTARG};;
		o) OutName=${OPTARG};;
		h) help;;
		k) keep="Y";;
		*) echo "Error: Bad usage"; usage;;
	esac
done
	#Test if -c -g -n arguments are filled
if [ -z $Chr ] || [ -z $GTF ] || [ -z $OutName ]
then
	echo "Error: Missing mandatory argument(s)"; usage
fi

#Main program
script=`readlink -f $0`
source=`dirname $script` #get path to pipeline's directory
mkdir -p Splicing_TMP #temp file folder
mkdir -p $OutName/$Chr #Output folder
ExonGFF="$OutName"_"$Chr"_exon.gff3 #exon.gff3's name
IntronGFF="$OutName"_"$Chr"_intron.gff3 #intron.gff3's name
SiteGFF="$OutName"_"$Chr"_splice_sites.gff3 #splice_sites.gff3's name
echo "Generating files for: $Chr"
echo "------"

	#Exon
awk -v C=$Chr '$1 ~ C {print $0}' $GTF | awk '$3 ~ /exon/ {print $0}' | sort -k4,4n | sed -e 's/+/@/g' | sed -e 's/-/+/g' | sed -e 's/@/-/g' > Splicing_TMP/exon.gtf #extract exon of Chr from GTF | sort by Start | inverse + and - strands
		#test presence of annotation for $Chr
annot=`more Splicing_TMP/exon.gtf | wc -l` #number of exon annotation for Chr in the GTF
if [ $annot == 0 ] #If no exon annotation for Chr
then
	echo "Error: Wrong chromosome name or No exon annotation for this chromosome"
	rm -r Splicing_TMP; rmdir $OutName/$Chr; usage #delete temp folder and empty output folder
fi
		#exon.gff3
cut -f9 Splicing_TMP/exon.gtf | cut -d";" -f1 | sed -e 's/gene_id /Parent=/g' | sed -e 's/"//g' | awk '{print NR "\t" $0}' > Splicing_TMP/Parent #extract <gene_id="xxx"> form 9th GTF field and save it as <NR \t Parent=xxx>
cut -f-8 Splicing_TMP/exon.gtf | awk '{print NR "\t" $0}' > Splicing_TMP/exon.tmp #add NR in fist field
join Splicing_TMP/exon.tmp Splicing_TMP/Parent | awk 'OFS="\t" {print $2,$3,$4,$5,$6,$7,$8,$9,$10}' > $OutName/$Chr/$ExonGFF #replace 9th field in exon.gtf by Parent and Print the table
rm Splicing_TMP/exon.tmp
echo "$ExonGFF : printed in ./$OutName/$Chr/"
echo -e "\t `more $OutName/$Chr/$ExonGFF | wc -l` exons" 

	#Intron
cut -f9 Splicing_TMP/exon.gtf | grep "exon_number \"2\"" | cut -d";" -f2 | sed -e 's/ transcript_id //g' | sed -e 's/"//g' > Splicing_TMP/intronic_gene #Parent of intron containing gene
grep -f Splicing_TMP/intronic_gene Splicing_TMP/exon.gtf > Splicing_TMP/intronic_gene.exon #extract exon of intron containing gene
R --no-save < $source/Toolbox/Splicing_Exon-Intron.R > R.log #Compute intron coordinates: input=intronic_gene.exon ; output=intron.tmp
rm R.log
sed -e 's/exon/intron/g' Splicing_TMP/intron.tmp > $OutName/$Chr/$IntronGFF #change feature to intron and mouv in Output folder 
echo "$IntronGFF : printed in ./$OutName/$Chr/"
echo -e "\t `more $OutName/$Chr/$IntronGFF | wc -l` introns" 

	#Splice_sites
R --no-save < $source/Toolbox/Splicing_Intron-Sites.R > R.log #Compute splice sites coordinates: input=intron.tmp ; output=splice_sites.tmp
sed -e 's/exon/splice_site/g' Splicing_TMP/splice_sites.tmp > $OutName/$Chr/$SiteGFF #change feature to splice_site and mouv in Output folder 
rm R.log
echo "$SiteGFF : printed in ./$OutName/$Chr/"
echo -e "\t `more $OutName/$Chr/$SiteGFF | wc -l` splice sites" 

	#End
if [ $keep == "N" ]
then
	rm -r Splicing_TMP #remove temp files
else
	echo -e "------\ntemporary files kept"
fi
echo -e "------\n"
