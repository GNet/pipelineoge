#Convert organellar mapped reads bed files into organellar coverage tables 
##set up ####
genome_size=read.table("LengthChr") #row1=Pt ; row2=Mt
filePt=system("ls *chloro.bed",T)
fileMt=system("ls *mito.bed",T)

##Plastid ####
data <- read.table(filePt, sep="\t")
colnames(data) <- c("Pt", "pos", "strand")
i_plus  <- which(data$strand == "-")
i_minus <- which(data$strand == "+")
tmp <- data.frame( #Coverage table creation
  pos	  = 0:genome_size[1,],
  Minus = tabulate(data$pos[i_minus]+1, nbins=genome_size[1,]+1),
  Plus  = tabulate(data$pos[i_plus]+1, nbins=genome_size[1,]+1))
colnames(tmp)<-c("pos",gsub("_chloro.bed","_minus",filePt),gsub("_chloro.bed","_plus",filePt))
filename=paste0("Counts/",gsub("_chloro.bed","_chloro_coverage.txt",filePt))
write.table(tmp, file=filename,sep="\t",quote=F,row.names=F,col.names=T) #print sample coverage table
rm(data,tmp)

##Mitochondrion ####
data <- read.table(fileMt, sep="\t")
colnames(data) <- c("Pt", "pos", "strand")
i_plus  <- which(data$strand == "-")
i_minus <- which(data$strand == "+")
tmp <- data.frame( #Coverage table creation
  pos	  = 0:genome_size[2,],
  Minus = tabulate(data$pos[i_minus]+1, nbins=genome_size[2,]+1),
  Plus  = tabulate(data$pos[i_plus]+1, nbins=genome_size[2,]+1))
colnames(tmp)<-c("pos",gsub("_mito.bed","_minus",fileMt),gsub("_mito.bed","_plus",fileMt))
filename=paste0("Counts/",gsub("_mito.bed","_mito_coverage.txt",fileMt))
write.table(tmp, file=filename,sep="\t",quote=F,row.names=F,col.names=T) #print sample coverage table
rm(data,tmp)
