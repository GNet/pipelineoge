#!/bin/bash
LOG=pipelineMC_OGE.log #logfile
(
script=`readlink -f $0`
source=`dirname $script` #get path to pipeline's directory
sourcefolder=`dirname $source`
echo -e "\n\t|---> Organellar Gene Expression Pipeline <---|\n"

#Get user intructions + sample listing
echo -e "# Sample selection :\n"
echo "sampleFile is a tabulated file containing name and path for each sample to analyse."
while [ -z $sampleFile ]
do
	read -p "sampleFile's name : " sampleFile #Ask sampleFile's name
	echo ""
done
echo -e "\n# Sample List : \n"
awk 'ORS=" " {print $1}' $sampleFile #list of sampleName in sampleFile 
sampleLength=`awk '{print $1}' $sampleFile | wc -l` #number of sample in sampleFile
echo -e "\n\nsample number to parse : $sampleLength "

echo -e "\n------\n\n# Mapping-Counting options :\n"
while [ -z $Mapping ]
do
	read -p "Perform Mapping and Counting <y/n> : " Mapping
	echo ""
done
until [ $Mapping == "y" ] || [ $Mapping == "n" ]
do 
	read -p "Perform Mapping and Counting <y/n> : " Mapping
	echo ""
done
if [ $Mapping == "y" ] #Options for Mapping-Counting
then
	#datatype
	echo -e "Single-end (=se) or Paired-end (=pe) data ?"
	while [ -z $datatype ]
	do
		read -p "type of data sequencing <se/pe> : " datatype
		echo ""
	done
	until [ $datatype == "se" ] || [ $datatype == "pe" ]
	do
		read -p "type of data sequencing <se/pe> : " datatype
		echo ""
	done
	#keepBAM
	while [ -z $keepBAM ]
	do
		read -p "Keep the BAMs (file.bam and file.bam.bai) <y/n> : " keepBAM
		echo ""
	done
	until [ $keepBAM == "y" ] || [ $keepBAM == "n" ]
	do 
		read -p "Keep the BAMs (file.bam and file.bam.bai) <y/n> : " keepBAM
		echo ""
	done
	#ThreadNB
	while [ -z $ThreadNB ]
	do
		read -p "Number of thread to use for the mapping : " ThreadNB
		echo ""
	done
	#indexID
	echo -e "Reference set selection :\n"
	awk -F"\t" '{print $1 "\t" $2 "\t" $3}' $source/indexList.txt #print list of index
	echo "------"
	echo "Choose the reference set to use for this analysis. It will be used for all samples listed in the sampleFile."
	while [ -z $indexID ]
	do
		read -p "genome index's ID : " indexID
		echo ""
	done
	#Set up
	mkdir -p Counts #featurecounts, splicing, editing, coverage
	mkdir -p BAMfiles #bam
	#extract reference set data from indexList
	pathIndex=`sed -e 's,$sourcefolder,'$sourcefolder',g' $source/indexList.txt | awk -v ID=$indexID -F"\t" '$1 == ID {print $4}'` #path to STARindex =mapping
	pathGTF=`sed -e 's,$sourcefolder,'$sourcefolder',g' $source/indexList.txt | awk -v ID=$indexID -F"\t" '$1 == ID {print $5}'` #path to GTF =counting
	pathFA=`sed -e 's,$sourcefolder,'$sourcefolder',g' $source/indexList.txt | awk -v ID=$indexID -F"\t" '$1 == ID {print $6}'` #path to Fasta =editing
	pathSplicing=`sed -e 's,$sourcefolder,'$sourcefolder',g' $source/indexList.txt | awk -v ID=$indexID -F"\t" '$1 == ID {print $7}'` #path to splicing files =splicing
	ChrPt=`awk -v ID=$indexID -F"\t" '$1 == ID {print $8}' $source/indexList.txt | cut -d"/" -f1` #Name of the Plastidial chromosome
	ChrMt=`awk -v ID=$indexID -F"\t" '$1 == ID {print $8}' $source/indexList.txt | cut -d"/" -f2` #Name of the Mitochondrial chromosome
	#Chromosome length
	samtools dict "$pathFA" | grep $ChrPt | cut -f3 | cut -d":" -f2 > LengthChr #Get plastidial chromosome length
	samtools dict "$pathFA" | grep $ChrMt | cut -f3 | cut -d":" -f2 >> LengthChr #Get mitochondrial chromosome length
	#Path to splicing files
	exonPt=`ls "$pathSplicing"/$ChrPt/*exon.gff3` #Pt_exon.gff3
	intronPt=`ls "$pathSplicing"/$ChrPt/*intron.gff3` #Pt_intron.gff3
	splice_sitePt=`ls "$pathSplicing"/$ChrPt/*splice_sites.gff3` #Pt_splice_sites_sort.gff3
	exonMt=`ls "$pathSplicing"/$ChrMt/*exon.gff3` #Mt_exon.gff3
	intronMt=`ls "$pathSplicing"/$ChrMt/*intron.gff3` #Mt_intron.gff3
	splice_siteMt=`ls "$pathSplicing"/$ChrMt/*splice_sites.gff3` #Mt_splice_sites_sort.gff3
fi

echo -e "\n------\n\n# Differential Analysis options :\n"
while [ -z $BioStat ]
do
	read -p "Perform Differential Analysis <y/n> : " BioStat
	echo ""
done
until [ $BioStat == "y" ] || [ $BioStat == "n" ]
do 
	read -p "Perform Differential Analysis <y/n> : " BioStat
	echo ""
done
if [ $BioStat == "y" ] #Options for DiffAnalysis
then
	#default behavior
	while [ -z $default ]
	do
		read -p "Use default behavior <y/n> : " default
		echo ""
	done
	until [ $default == "y" ] || [ $default == "n" ]
	do 
		read -p "Use default behavior <y/n> : " default
		echo ""
	done
	if [ $default == "n" ]
	then
		#CPM
		while [ -z $CPM ]
		do
			read -p "CPM cutoff, filtering threshold for DEG analysis (default: 1) : " CPM
			echo ""
		done
		#RepEffect
		while [ -z $RepEffect ]
		do
			read -p "add a replicate term in the GLM for DEG analysis (default: T) <T/F> : " RepEffect
			echo ""
		done
		until [ $RepEffect == "T" ] || [ $RepEffect == "F" ]
		do 
			read -p "add a replicate term in the GLM for DEG analysis (default: T) <T/F> : " RepEffect
			echo ""
		done
		#filtercoved (filter.cov ed)
		while [ -z $filtercoved ]
		do
			read -p "filter on coverage, minimum number of edited reads found across samples to keep the position (default: 6) : " filtercoved
			echo ""
		done
		#filterrateed (filter.rate ed)
		while [ -z $filterrateed ]
		do
			read -p "filter on editing rate, minimum average editing rate by condition to keep the position (default: 0.005) : " filterrateed
			echo ""
		done
		#filtercovspl (filter.cov spl)
		while [ -z $filtercovspl ]
		do
			read -p "filter on coverage, minimum number of spliced reads found across samples to keep the intron (default: 6) : " filtercovspl
			echo ""
		done
	fi
fi

#Summary
echo -e "\n------\n\n# Summary : \n"
echo -e "number of sample in $sampleFile : $sampleLength"
echo -e "\nMapping-Counting are performed : $Mapping"
if [ $Mapping == "y" ]
then
	echo -e "\ttype of data sequencing : $datatype"
	echo -e "\tBAMs are kept : $keepBAM"
	echo -e "\tnumber of thread use by STAR : $ThreadNB"
	echo -e "\treference genome used for the mapping is : $pathIndex"
	echo -e "\tSTAR version used to generate the index : `awk -v ID=$indexID -F"\t" '$1 == ID {print $9}' $source/indexList.txt`"
	echo -e "\tgtf used is : $pathGTF"
	echo -e "\tfasta used is : $pathFA"
	echo -e "\tsplicing files set is : $pathSplicing"
	echo -e "\tplastidial chromosome length ($ChrPt) : `awk 'NR==1 {print $0}' LengthChr`"
	echo -e "\tmitochondrial chromosome length ($ChrMt) : `awk 'NR==2 {print $0}' LengthChr`"
fi
echo -e "\nDifferential Analysis are performed : $BioStat"
if [ $BioStat == "y" ]
then
	echo -e "\tdefault behavior : $default"
	if [ $default == "n" ]
	then
		echo -e "\tchosen parameters :"
		echo -e "\t\tCPM cutoff: $CPM"
		echo -e "\t\treplicate term: $RepEffect"
		echo -e "\t\tfilter on coverage for editing analysis: $filtercoved"
		echo -e "\t\tfilter on editing rate: $filterrateed"
		echo -e "\t\tfilter on coverage for splicing analysis: $filtercovspl"
	fi
fi

#Mapping-Counting
if [ $Mapping == "y" ] #do mapping-counting
then
	echo -e "\n------\n\n# Mapping-Counting : \n"
	for i in `seq 1 $sampleLength`
	do #START-MC-loop sample by sample
		if [ $datatype == "pe" ] #get path to fastq(s)
		then #pe
			pathFQ=`awk -v i=$i 'NR==i, OFS=" " {print $0}' $sampleFile | cut -f2,3 --output-delimiter=" "`
		else #se
			pathFQ=`awk -v i=$i 'NR==i, OFS=" " {print $2}' $sampleFile`
		fi
		Name=`awk -v i=$i 'NR==i {print $1}' $sampleFile` #sampleName
		echo -e "--> Sample : $Name \n"

			#Mapping
		STAR --readFilesCommand gunzip -c  --outSAMprimaryFlag AllBestScore --outFilterMultimapScoreRange 0  --outSAMtype BAM SortedByCoordinate --runThreadN $ThreadNB --alignIntronMax 1 --outFileNamePrefix "$Name"_ --genomeDir "$pathIndex" --readFilesIn $pathFQ
		samtools index "$Name"_Aligned.sortedByCoord.out.bam

			#Counts
		featureCounts -M -s 2 -t exon -g gene_id -a "$pathGTF" -o Counts/"$Name"_featurecounts.txt "$Name"_Aligned.sortedByCoord.out.bam

			#Splicing
				#Pt
		intersectBed -abam "$Name"_Aligned.sortedByCoord.out.bam -b "$splice_sitePt" -s | intersectBed -abam stdin -b "$intronPt" | intersectBed -abam stdin -b "$exonPt" > splice_junctions.out
		samtools view -h splice_junctions.out | awk '$6 ~ /N/ || $1 ~ /^@/' | samtools view -bS - | coverageBed  -a "$intronPt" -b stdin | sort -k 4,4n | cut -f 4,5,10 > spliced_coverage.txt
		samtools view -h splice_junctions.out | awk '$6 !~ /N/ || $1 ~ /^@/' | samtools view -bS - |  coverageBed -a "$intronPt" -b stdin | sort -k 4,4n | cut -f 4,10 > unspliced_coverage.txt
		join spliced_coverage.txt unspliced_coverage.txt | awk '{print $1 "\t" $2 "\t" $3 "\t" $4}' > Counts/"$Name"_chloro_splicing.txt
		rm splice_junctions.out spliced_coverage.txt unspliced_coverage.txt
				#Mt
		intersectBed -abam "$Name"_Aligned.sortedByCoord.out.bam -b "$splice_siteMt" -s | intersectBed -abam stdin -b "$intronMt" | intersectBed -abam stdin -b "$exonMt" > splice_junctions.out
		samtools view -h splice_junctions.out | awk '$6 ~ /N/ || $1 ~ /^@/' | samtools view -bS - | coverageBed  -a "$intronMt" -b stdin | sort -k 4,4n | cut -f 4,5,10 > spliced_coverage.txt
		samtools view -h splice_junctions.out | awk '$6 !~ /N/ || $1 ~ /^@/' | samtools view -bS - |  coverageBed -a "$intronMt" -b stdin | sort -k 4,4n | cut -f 4,10 > unspliced_coverage.txt
		join spliced_coverage.txt unspliced_coverage.txt | awk '{print $1 "\t" $2 "\t" $3 "\t" $4}' > Counts/"$Name"_mito_splicing.txt
		rm splice_junctions.out spliced_coverage.txt unspliced_coverage.txt
		echo -e "\t--> Splicing count tables : printed"

			#Editing
		source $source/MyPython/bin/activate
		pysamstats -d -D 100000000 -f "$pathFA" -c $ChrPt --type variation_strand "$Name"_Aligned.sortedByCoord.out.bam > Counts/"$Name"_chloro_editing.txt
		pysamstats -d -D 100000000 -f "$pathFA" -c $ChrMt --type variation_strand "$Name"_Aligned.sortedByCoord.out.bam > Counts/"$Name"_mito_editing.txt
		deactivate
		echo -e "\t--> Editing count tables : printed"

			#Coverage
		bedtools bamtobed -i "$Name"_Aligned.sortedByCoord.out.bam | awk '{print $1 "\t" $2 "\t" $6}'| grep $ChrPt > "$Name"_chloro.bed
		bedtools bamtobed -i "$Name"_Aligned.sortedByCoord.out.bam | awk '{print $1 "\t" $2 "\t" $6}'| grep $ChrMt > "$Name"_mito.bed
		R --no-save < $source/Toolbox/BEDtoCoverage.R > R.log #convert .bed in coverage tables (aka Counts/"$Name"_choloro|mito_coverage.txt) 
		echo -e "\t--> Coverage count tables : printed"
		rm *bed

		#mouv BAM and STAR log
		mv *bam* ./BAMfiles/
		mv *Log.final.out ./BAMfiles/
		rm *out*
		echo -e "\n------\n"
	done #END-MC-loop

	#Merging Count tables
	echo -e "# Merging Count tables : \n"
	R --no-save < $source/Toolbox/MergeCountTable.R > R.log #merge count tables by analysis kind
	echo -e "\t--> Merged count tables : printed"

	#remove BAMfiles
	if [ $keepBAM == "n" ] #remove BAMfiles
	then
		rm BAMfiles/*.bam*
		echo -e "\n------\n\nBAMs deleted"
	fi
	rm LengthChr R.log
	rm -r *_STARtmp #delete STAR temp directories 
fi

#DiffAnalysis
if [ $BioStat == "y" ] #do diffanalysis
then
	echo -e "\n------\n\n# Differential Analysis :"
	if [ $default == "y" ]
	then
		$source/DiffAnalysis.sh -S $sampleFile
	fi
	if [ $default == "n" ]
	then
		$source/DiffAnalysis.sh -S $sampleFile -C $CPM -R $RepEffect -c $filtercoved -r $filterrateed -v $filtercovspl
	fi
fi

#END
echo -e "\n------\n\nFinish\n\n------\n"

) 2>&1| tee $LOG #create the log file
